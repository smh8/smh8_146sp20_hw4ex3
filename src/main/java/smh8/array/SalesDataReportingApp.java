import java.text.NumberFormat;
    
public class SalesDataReportingApp{
    public static void main(String[] args){
        //Setting up currency format
        NumberFormat currency = NumberFormat.getCurrencyInstance();

        //System.out.println("test");
        double salesRegionQuarter[][] = 
            {
            {1540.00, 2010.00, 2450.00, 1845.00},
            {1130.00, 1168.00, 1847.00, 1491.00},
            {1580.00, 2305.00, 2710.00, 1284.00},
            {1105.00, 4102.00, 2391.00, 1576.00}
            };
        
        //output for regions and quarters
        String header[] = {"Region", "Q1", "Q2", "Q3", "Q4"};
        
        for(int i=0; i<salesRegionQuarter.length+1; i++){
            for(int j=0; j<5; j++){
                if(i==0){
                    System.out.print(header[j]+"        ");
                } else if(j==0){
                    System.out.print(i+"             ");
                } else{
                    System.out.print(currency.format(salesRegionQuarter[i-1][j-1])+" ");
                }
            }
            System.out.println();
        }
        
        //output for regions with total for all quarters
        System.out.println("\nSales by Region:");
        double total;
        
        for(int i=0; i<salesRegionQuarter.length; i++){
            
            total=0;
            for(int j=0; j<salesRegionQuarter[i].length; j++){
                total=total+salesRegionQuarter[i][j];
            }
            System.out.println("Region "+(i+1)+": "+currency.format(total));
        }
        
        //output for each quarter with total for all regions
        System.out.println("\nSales by Quarter:");
        
        for(int j=0; j<salesRegionQuarter[0].length; j++){
            
            total=0;
            for(int i=0; i<salesRegionQuarter.length; i++){
                total=total+salesRegionQuarter[i][j];
            }
            System.out.println("Quarter "+(j+1)+": "+currency.format(total));
        }
        
        //output for total for all regions and quarters
        total=0;
        
        for(double[] i : salesRegionQuarter){
            for(double j : i){
                total=j+total;
            }
        }
        System.out.println("\nTotal annual sales, all regions: "+currency.format(total));
    }
}